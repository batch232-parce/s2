package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {
    public static void main(String[] args) {
        // Operators in Java
        // Arithmetic- +, -, *, /, %
        // Comparison- >, <, >=, <=, ==, !=
        // Logical- &&, ||, !
        // Assignment- =

        // Conditional Structures in Java
        // statement allows us to manipulate the flow of the code depending on the evaluation of the condition
        // Syntax: if(condition){
        // }
        int num1 = 15;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }

        // else statement will allow us to run a task or code if the condition fails or have a falsy values
        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }
        /*
          Mini-Activity:
          1. Create/instantiate a new Scanner object from the Scanner class and name it as numberScanner.
          2. Ask the user for an integer and store the input in a variable.
          3. Add an if-else statement:
             a. IF the number is even, show a message:
                    num is even!
             b. Else, show the following message:
                    num is odd!
         4. Send your output screenshots in Hangouts
        */
        // SOLUTION:
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
//        int numEvenOdd = numberScanner.nextInt();
//        if(numEvenOdd % 2 == 0){
//            System.out.println(numEvenOdd + " is even!");
//        } else {
//            System.out.println(numEvenOdd + " is odd!");
//        }
        // short-circuiting
        // a short circuit happens because the result is clear even before the complete evaluation of the expression, and the result is returned.
        int x = 15;
        int y = 0;
        if(y == 0 || x == 15 || y != 0) System.out.println(true);

        // ternary operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true :false;
        // alt solution
        // String result = (number > 0) ? Boolean.toString(true) : Boolean.toString(false);
        System.out.println(result);

        // Switch Cases
        // control flow structures that allow one code block to be run out of many other code block
        // this is often when the input is predictable

        System.out.println("Enter a number from 1-4 to see a SM Malls in one of the four directions");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("SM North EDSA");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of Range");
        }
    }
}
