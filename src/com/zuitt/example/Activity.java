package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main (String[] args){
        String[] arrFruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(arrFruits));

        Scanner getFruit = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String fruitName = getFruit.nextLine();
        System.out.println("The index of " + fruitName + " is: " + Arrays.binarySearch(arrFruits, fruitName));

        ArrayList<String> arrFriends = new ArrayList<>(Arrays.asList("Shanneille", "Grace", "Zsareena", "Rhys"));
        System.out.println("My friends are: " + arrFriends);

        HashMap<String,Integer> arrProducts = new HashMap<>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + arrProducts);

    }
}
